﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.Foundation.MVC;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class BaseController : Controller
    {
        protected JsonNetResult JsonNet(object data)
        {
            var ret = new JsonNetResult(data);
            return ret;
        }

        public UserModel CurrentUser
        {
            get
            {
                if (User != null 
                    &&User.Identity!=null 
                    &&! string.IsNullOrEmpty(User.Identity.Name))
                {
                    var jsonStr = User.Identity.Name.DESDecrypt();
                    var ret = Newtonsoft.Json.JsonConvert.DeserializeObject<UserModel>(jsonStr);
                    return ret;
                }
                else {
                    return new UserModel();
                }
            }
        }
    }
}
