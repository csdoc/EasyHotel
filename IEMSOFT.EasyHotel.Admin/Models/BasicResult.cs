﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class BasicResult<TData,TMsg>
    {
        public TData Data { get; set; }
        public TMsg Msg { get; set; }
    }
}