﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class BillFullModel : BillModel
    {
        public string RoomNo { get; set; }
        public string TravelAgencyName { get; set; }
        public string PayTypeName { get; set; }
        public string CreateUserFullName { get; set; }
        public string UpdateUserFullName { get; set; }
        public string RoomTypeName { get; set; }
        public int SubHotelId { get; set; }
        public int RoomTypeId { get; set; }
        public string SubHotelName { get; set; }
        public string IsHourRoomStr { get {
            if (IsHourRoom.ToBool())
                return "是";
            else
                return "否";
        } }

        public string MinFeeForHourRoomStr
        {
            get
            {
                if (IsHourRoom.ToBool())
                    return MinFeeForHourRoom.ToString();
                else
                    return "";
            }
        }

        public int TotalDaysOrHours
        {
            get
            {
                if (IsHourRoom.ToBool())
                    return StayTotalHours;
                else
                    return StayTotalDays;
            }
        }

        public decimal PriceForDayOrHour
        {
            get
            {
                if (IsHourRoom.ToBool())
                    return PricePerHour;
                else
                    return SinglePrice;
            }
        }
    }
}