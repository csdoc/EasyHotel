﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL.Models
{
    [PetaPoco.TableName("Role")]
    [PetaPoco.PrimaryKey("RoleId",autoIncrement=true)]
  public  class Role
    {
        public int RoleId { get; set; }
        public String Name { get; set; }
        private DateTime _created = DateTime.Now;
        public DateTime Created { get { return _created; } set { _created = value; } }
        private DateTime _modified = DateTime.Now;
        public DateTime Modified { get { return _modified; } set { _modified = value; } }
    }
}
