﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.DAL
{
    public class RoleDAL
    {
        private  IDBProvider _dbProvider;
        public RoleDAL(IDBProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        public List<Role> GetList()
        {
            var ret = _dbProvider.DB.Fetch<Role>("select * from Role");
           return ret??new List<Role>();
        }

        public int GetRoleIdByName(string roleName)
        {
            var ret = _dbProvider.DB.FirstOrDefault<int>("select * from Role where Name=@0",roleName);
            return ret;
        }
    }
}
